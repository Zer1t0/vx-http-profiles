import argparse
import json
from abc import ABC, abstractmethod

PAYLOAD_PLACEHOLDER = "message"

def parse_args():
	parser = argparse.ArgumentParser()

	parser.add_argument("config")

	return parser.parse_args()

def main():
	args = parse_args()

	with open(args.config) as fi:
		config = json.load(fi)


	ba = bytearray()

	get_agent_config = config["GET"]["AgentMessage"][0]
	encode_agent_message(ba, get_agent_config)

	post_agent_config = config["POST"]["AgentMessage"][0]
	encode_agent_message(ba, post_agent_config)

	encode_server_message(ba, config["GET"])
	encode_server_message(ba, config["POST"])

	print("#define HTTP_CONFIG %s" % bytes_to_c_array(ba))

def encode_server_message(ba, server_config):
	encode_server_body(ba, server_config["ServerBody"])
	encode_server_headers(ba, server_config["ServerHeaders"])
	encode_server_cookies(ba, server_config["ServerCookies"])
	
def encode_server_body(ba, server_body):
	is_payload_in_body = 1
	ba.extend(uint8_to_bytes(is_payload_in_body))
	encode_reverse_transforms(ba, server_body)

def encode_server_headers(ba, server_headers):
	is_payload_in_header = 0
	ba.extend(uint8_to_bytes(is_payload_in_header))

def encode_server_cookies(ba, server_cookies):
	"""
	for cookie in server_cookies:
		if cookie["value"] == PAYLOAD_PLACEHOLDER:
			is_payload_in_cookie = 1
			ba.extend(uint8_to_bytes(is_payload_in_cookie))
			ba.extend(string_to_c(cookie["name"]))
			encode_reverse_transforms(ba, cookie["transforms"])
			return
	"""
	is_payload_in_cookie = 0
	ba.extend(uint8_to_bytes(is_payload_in_cookie))

def encode_agent_message(ba, agent_config):
	encode_uri(
		ba, 
		agent_config["uri"], 
		agent_config.get("urlFunctions", [])
	)
	encode_params(ba, agent_config.get("QueryParameters", []))
	encode_headers(ba, agent_config.get("AgentHeaders", {}))
	encode_params(ba, agent_config.get("Cookies", []))
	encode_body(ba, agent_config.get("Body", []))

def bytes_to_c_array(bs):
	return "{ %s }" % ", ".join([hex(b) for b in bs])

def encode_body(ba, transforms):
	if transforms:
		use_payload = 1
	else:
		use_payload = 0
	ba.extend(uint8_to_bytes(use_payload))
	encode_transforms(ba, transforms)

def encode_uri(ba, uri, uri_functions):
	ba.extend(string_to_c(uri))
	encode_params(ba, uri_functions)

def encode_headers(ba, headers):
	headers_list = [
		{"name": name, "value": value, "transforms": []} 
		for name,value in headers.items()
	]
	encode_params(ba, headers_list)

def encode_params(ba, params):
	params_count = len(params)
	ba.extend(uint16_to_bytes(params_count))

	for param in params:
		encode_param(ba, param)

def encode_param(ba, param):
	ba.extend(string_to_c(param["name"]))

	use_payload = param["value"] == PAYLOAD_PLACEHOLDER
	ba.extend(uint8_to_bytes(use_payload))
	ba.extend(string_to_c(param["value"]))
	encode_transforms(ba, param["transforms"])

def encode_transforms(ba, transforms):
	transforms_count = len(transforms)
	ba.extend(uint8_to_bytes(transforms_count))
	for transform in transforms:
		encode_transform(ba, transform)

def encode_reverse_transforms(ba, transforms):
	transforms_count = len(transforms)
	ba.extend(uint8_to_bytes(transforms_count))
	for transform in reversed(transforms):
		encode_reverse_transform(ba, transform)

APPEND_CODE = 1
RANDOM_ALPHA_CODE = 2
BASE64_CODE = 3
CHOOSE_RANDOM_CODE = 4
PREPEND_CODE = 5

class TransformEncoder(ABC):

	@classmethod
	@abstractmethod
	def code(cls):
		pass
		
	@classmethod
	@abstractmethod
	def encode_parameters(cls, ba, parameters):
		pass

	@classmethod
	@abstractmethod
	def encode_reverse_parameters(cls, ba, parameters):
		pass


class RandomAlphaEncoder(TransformEncoder):

	@classmethod
	def code(cls):
		return RANDOM_ALPHA_CODE

	@classmethod
	def encode_parameters(cls, ba, parameters):
		ba.extend(uint32_to_bytes(parameters[0]))

	@classmethod
	def encode_reverse_parameters(cls, ba, parameters):
		ba.extend(uint32_to_bytes(parameters[0]))


class Base64Encoder(TransformEncoder):

	@classmethod
	def code(cls):
		return BASE64_CODE

	@classmethod
	def encode_parameters(cls, ba, parameters):
		pass

	@classmethod
	def encode_reverse_parameters(cls, ba, parameters):
		pass

class ChooseRandomEncoder(TransformEncoder):
	
	@classmethod
	def code(cls):
		return CHOOSE_RANDOM_CODE

	@classmethod
	def encode_parameters(cls, ba, parameters):
		params_count = len(parameters)
		ba.extend(uint8_to_bytes(params_count))
		for param in parameters:
			ba.extend(string_to_c(param))

	@classmethod
	def encode_reverse_parameters(cls, ba, parameters):
		pass

class AppendEncoder(TransformEncoder):
	
	@classmethod
	def code(cls):
		return APPEND_CODE

	@classmethod
	def encode_parameters(cls, ba, parameters):
		ba.extend(string_to_c(parameters[0]))

	@classmethod
	def encode_reverse_parameters(cls, ba, parameters):
		ba.extend(uint32_to_bytes(len(parameters[0])))

class PrependEncoder(TransformEncoder):
	
	@classmethod
	def code(cls):
		return PREPEND_CODE

	@classmethod
	def encode_parameters(cls, ba, parameters):
		ba.extend(string_to_c(parameters[0]))

	@classmethod
	def encode_reverse_parameters(cls, ba, parameters):
		ba.extend(uint32_to_bytes(len(parameters[0])))

			
	
TRANSFORM_ENCODERS = {
	"append": AppendEncoder,
	"random_alpha": RandomAlphaEncoder,
	"base64": Base64Encoder,
	"choose_random": ChooseRandomEncoder,
	"prepend": PrependEncoder,
}

def encode_transform(ba, t):
	transform_encoder = TRANSFORM_ENCODERS[t["function"]]
	ba.extend(uint16_to_bytes(transform_encoder.code()))
	transform_encoder.encode_parameters(ba, t["parameters"])
	
def encode_reverse_transform(ba, t):
	transform_encoder = TRANSFORM_ENCODERS[t["function"]]
	ba.extend(uint16_to_bytes(transform_encoder.code()))
	transform_encoder.encode_reverse_parameters(ba, t["parameters"])
	

def uint8_to_bytes(u):
	return u.to_bytes(1, "little")

def uint16_to_bytes(u):
	return u.to_bytes(2, "little")

def uint32_to_bytes(u):
	return u.to_bytes(4, "little")

def string_to_c(s):
	return s.encode() + b"\x00"

if __name__ == "__main__":
	exit(main())