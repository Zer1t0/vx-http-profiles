import argparse
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import base64
from functools import partial
from urllib.parse import urlparse, parse_qs
import http.cookies

APPEND_CODE = 1
RANDOM_ALPHA_CODE = 2
BASE64_CODE = 3
CHOOSE_RANDOM_CODE = 4
PREPEND_CODE = 5

http_pipes = None

GET_PAYLOAD = b"Hello from GET server"
POST_PAYLOAD = b"Hello from POST server"

PAYLOAD_PLACEHOLDER = "message"

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("config")
    parser.add_argument(
        "-b", "--bind",
        help="Bind address",
        default="127.0.0.1",
    )
    
    parser.add_argument(
        "-p", "--port",
        help="Port to listen",
        default=8000,
        type=int,
    )
    
    return parser.parse_args()


def main():
    global http_pipes
    args = parse_args()
    bind_host = args.bind
    bind_port = args.port

    with open(args.config) as fi:
        config = json.load(fi)
        
    http_pipes = parse_config(config)

    print("Listening in http://{}:{}/".format(bind_host, bind_port))
    server = HTTPServer((bind_host, bind_port), MyServer)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.shutdown()
    finally:
        server.server_close()
        
    
    
class ParamPipeline:

    def __init__(self, name, initial_value=b"", use_payload=False, transforms=None):
        self.name = name
        self.initial_value = initial_value
        self.use_payload = use_payload
        self.transforms = transforms or []
        
    def process(self, payload):
        value = payload if self.use_payload else self.initial_value
        for t in self.transforms:
            value = t(value)            
        return value
      

class ServerResponser:

    def __init__(
        self, 
        body_pipe: ParamPipeline, 
        headers_pipe: ParamPipeline, 
        cookies_pipe: ParamPipeline,
    ):
        self.body_pipe = body_pipe
        self.headers_pipe = headers_pipe
        self.cookies_pipe = cookies_pipe
        
class AgentDecoder:
    def __init__(
        self, 
        uri_decoder: ParamPipeline, 
        url_param_decoder: ParamPipeline, 
        body_decoder: ParamPipeline, 
        header_decoder: ParamPipeline, 
        cookie_decoder: ParamPipeline,
    ):
        self.uri_decoder = uri_decoder
        self.url_param_decoder = url_param_decoder
        self.body_decoder = body_decoder 
        self.header_decoder = header_decoder
        self.cookie_decoder = cookie_decoder
             
class HttpPipes:

    def __init__(
        self, 
        get_request: AgentDecoder, 
        post_request: AgentDecoder, 
        get_response: ServerResponser, 
        post_response: ServerResponser,
    ):
        self.get_request = get_request
        self.post_request = post_request
        self.get_response = get_response
        self.post_response = post_response
        
 

def transform_append(parameters, value):
    return value + parameters[0]
    
def reverse_transform_append(parameters, value):
    return value[:-len(parameters[0])]
        
def transform_prepend(parameters, value):
    return parameters[0] + value
    
def reverse_transform_prepend(parameters, value):
    return value[len(parameters[0]):]
        
def transform_base64(parameters, value):
    return base64.b64encode(value)
    
def reverse_transform_base64(parameters, value):
    return base64.b64decode(value)
    
def reverse_transform_random_alpha(parameters, value):
    return reverse_transform_append(parameters, value)
 
       
def parse_config(config):
    get_response = parse_server(config["GET"])
    post_response = parse_server(config["POST"])
    get_request = parse_agent(config["GET"]["AgentMessage"][0])
    post_request = parse_agent(config["POST"]["AgentMessage"][0])
    
    return HttpPipes(get_request, post_request, get_response, post_response)
    
def parse_agent(agent_config):
    uri_decoder = parse_agent_uri(agent_config["uri"], agent_config["urlFunctions"])
    url_param_decoder = parse_agent_url_params(agent_config["QueryParameters"])
    header_decoder = parse_agent_headers(agent_config["AgentHeaders"])
    cookie_decoder = parse_agent_cookies(agent_config["Cookies"])
    body_decoder = parse_agent_body(agent_config["Body"])
    
    return AgentDecoder(
        uri_decoder=uri_decoder,
        url_param_decoder=url_param_decoder,
        header_decoder=header_decoder,
        cookie_decoder=cookie_decoder,
        body_decoder=body_decoder,
    )
    
def parse_agent_uri(uri, url_functions):
    return None
    
def parse_agent_url_params(query_parameters):
    for param in query_parameters:
        if param["value"] == PAYLOAD_PLACEHOLDER:
            name = param["name"]
            transforms = parse_reverse_transforms(param["transforms"])
            return ParamPipeline(
                name=name, 
                use_payload=True, 
                transforms=transforms
            )
    return None
    
def parse_agent_headers(headers):
    return None
   
def parse_agent_cookies(cookies):
    return None
    
def parse_agent_body(body):
    transforms = parse_reverse_transforms(body)
    return ParamPipeline(name="", use_payload=True, transforms=transforms)
   
   
def parse_reverse_transforms(transforms):
    funcs = []
    for t in reversed(transforms):
        func_name = t["function"]
        if func_name == "base64":
            func = reverse_transform_base64
        elif func_name == "prepend":
            func = reverse_transform_prepend
        elif func_name == "append":
            func = reverse_transform_append
        elif func_name == "random_alpha":
            func = reverse_transform_random_alpha
         
        parameters = [p.encode() for p in t["parameters"]]
        funcs.append(partial(func, parameters))
    return funcs
    
def parse_server(server_config):
    body_pipe = parse_server_body(server_config["ServerBody"])
    headers_pipe = parse_server_headers(server_config["ServerHeaders"])
    cookies_pipe = parser_server_cookies(server_config["ServerCookies"])
    return ServerResponser(body_pipe, headers_pipe, cookies_pipe)
    
def parse_server_body(server_body):
    transforms = parse_transforms(server_body)
    return ParamPipeline(
        name="",
        use_payload=True, 
        initial_value=b"", 
        transforms=transforms
    )
    
def parse_transforms(transforms):
    funcs = []
    for t in transforms:
        func_name = t["function"]
        if func_name == "base64":
            func = transform_base64
        elif func_name == "prepend":
            func = transform_prepend
        elif func_name == "append":
            func = transform_append
         
        parameters = [p.encode() for p in t["parameters"]]
        funcs.append(partial(func, parameters))
    return funcs

def parse_server_headers(server_headers):
    return [
        ParamPipeline(name=name, initial_value=value.encode()) 
        for name, value in server_headers.items()
    ]

def parser_server_cookies(server_cookies):
    return []
    """
    return [
        ParamPipeline(
            name=cookie["name"], 
            initial_value=cookie["value"].encode(),
            use_payload=cookie["value"] == PAYLOAD_PLACEHOLDER,
            transforms=parse_transforms(cookie["transforms"]),
            ) 
        for cookie in server_cookies
    ]
    """
    
class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        payload = parse_request(self, http_pipes.get_request)
        print("Payload from agent: {}".format(payload))

        self.send_response(200)
        write_response(self, http_pipes.get_response, b"Server GET: " + payload)

    def do_POST(self):
        payload = parse_request(self, http_pipes.post_request)
        print("Payload from agent: {}".format(payload))
        
        self.send_response(200)
        write_response(self, http_pipes.post_response, b"Server POST: " + payload)

def parse_request(handler, decoder):
    # read content body, if any, to avoid letting data in the request buffer
    if "Content-Length" in handler.headers:
        content_length = int(handler.headers['Content-Length'])
        body_data = handler.rfile.read(content_length)
    else:
        body_data = b""
    
    url = urlparse(handler.path)
    parameters = parse_qs(url.query)
    if decoder.url_param_decoder:
        param_name = decoder.url_param_decoder.name
        return decoder.url_param_decoder.process(
            parameters[param_name][0].encode()
        )
        
    if decoder.body_decoder:
        return decoder.body_decoder.process(body_data)
    

def write_response(handler, responser, payload):
    body = responser.body_pipe.process(payload)
    for header_pipe in responser.headers_pipe:
        name = header_pipe.name
        value = header_pipe.process(payload)
        handler.send_header(name, value.decode())
    
    cookies = http.cookies.SimpleCookie()
    for cookie_pipe in responser.cookies_pipe:
        value = cookie_pipe.process(payload).decode()
        print(value)
        cookies[cookie_pipe.name] = value
    
    for c in cookies.values():
        handler.send_header("Set-Cookie", c.OutputString())
    
    if body:
        handler.send_header("Content-Length", str(len(body)))
    handler.end_headers()
    
    if body:
        handler.wfile.write(body)

if __name__ == "__main__":
    exit(main())