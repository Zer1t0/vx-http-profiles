

#include "basic_utils.h"
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include "alloc.h"
#include <bcrypt.h>

unsigned char* copy_data(const unsigned char* input, size_t input_size) {
	unsigned char* data = MALLOC(input_size);
	assert(data);
	memcpy(data, input, input_size);
	return data;
}

char* copy_string(const char* str) {
	size_t str_size = strlen(str) + 1;
	return copy_data(str, str_size);
}

wchar_t* copy_wstring(const wchar_t* wstr) {
	size_t wstr_size = (wcslen(wstr) + 1) * sizeof(wchar_t);
	return (wchar_t*) copy_data((const unsigned char*) wstr, wstr_size);
}

static char* replace_string_from(size_t index, const char* str, const char* old_value, const char* new_value) {
	char* old_value_ptr = NULL;
	size_t old_start = 0;
	size_t old_end = 0;
	size_t start_part_size = 0;
	size_t end_part_size = 0;
	size_t new_value_len = 0;
	size_t new_start_index = 0;
	char* new_str = NULL;
	char* new_str_ptr = NULL;
	char* next_new_str = NULL;

	old_value_ptr = strstr(str + index, old_value);
	if (!old_value_ptr) {
		return copy_string(str);
	}

	old_start = old_value_ptr - str;
	old_end = old_start + strlen(old_value);

	start_part_size = old_start;
	end_part_size = strlen(str) - old_end;

	new_value_len = strlen(new_value);
	new_str = CALLOC(start_part_size + strlen(new_value) + end_part_size + 1);
	assert(new_str);

	new_str_ptr = new_str;
	memcpy(new_str_ptr, str, start_part_size);
	new_str_ptr += start_part_size;
	memcpy(new_str_ptr, new_value, new_value_len);
	new_str_ptr += new_value_len;
	memcpy(new_str_ptr, str + old_end, end_part_size);

	new_start_index = old_start + new_value_len;

	next_new_str = replace_string_from(new_start_index, new_str, old_value, new_value);
	DESTROY(new_str);
	return next_new_str;
}

char* replace_string(const char* str, const char* old_value, const char* new_value) {
	return replace_string_from(0, str, old_value, new_value);
}

void concat_string(char** str1, const char* str2) {
	size_t new_size = 0;
	char* tmp = NULL;

	new_size = strlen(*str1) + strlen(str2) + 1;
	tmp = RECALLOC(*str1, new_size);
	assert(tmp);
	*str1 = tmp;
	strcat(*str1, str2);
}

void append(
	const unsigned char* input1, size_t input1_size,
	const unsigned char* input2, size_t input2_size,
	unsigned char** output, size_t* output_size
) {
	size_t new_size = 0;
	unsigned char* result = NULL;

	new_size = input1_size + input2_size;
	result = MALLOC(new_size);
	memcpy(result, input1, input1_size);
	memcpy(result + input1_size, input2, input2_size);

	*output = result; result = NULL;
	*output_size = new_size;
}

int randomnize(unsigned char* data, size_t size) {
	return BCryptGenRandom(NULL, data, (ULONG)size, BCRYPT_USE_SYSTEM_PREFERRED_RNG);
}

wchar_t* to_unicode(const char* ascii) {
	wchar_t* unicode = CALLOC((strlen(ascii) + 1) * 2);
	assert(unicode);

	mbstowcs(unicode, ascii, strlen(ascii));
	return unicode;
}


char* to_ascii(const wchar_t* unicode) {
	char* ascii = CALLOC(wcslen(unicode) + 1);
	assert(ascii);

	if (wcstombs(ascii, unicode, wcslen(unicode)) == -1) {
		DESTROY(ascii);
	}
	return ascii;
}