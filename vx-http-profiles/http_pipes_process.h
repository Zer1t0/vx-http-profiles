#ifndef HTTP_PIPES_PROCESS_H
#define HTTP_PIPES_PROCESS_H

#include "http_pipes.h"
#include "http.h"

http_request_options_t* process_http_payload(
	const char* method,
	http_pipes_t* hpp,
	const unsigned char* payload,
	const size_t payload_size
);

buffer_t* process_http_response(
	char* method,
	http_response_t* response,
	http_pipes_t* hpp
);

#endif