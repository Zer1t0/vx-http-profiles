
#include "data_parser.h"
#include "alloc.h"

#include <stdint.h>
#include <assert.h>

void data_parser_destroy(data_parser_t** dp) {
	buffer_destroy(&(*dp)->buffer);
	DESTROY(*dp);
}

data_parser_t* data_parser_from_bytes_copy(unsigned char* data, size_t size) {
	data_parser_t* dp = NULL;

	dp = MALLOC(sizeof(data_parser_t));
	assert(dp);

	dp->buffer = buffer_new_copy(data, size);
	dp->pos = 0;

	return dp;
}

int _dp_is_in_buffer_range(data_parser_t* dp, size_t size) {
	return dp->pos + size <= dp->buffer->size;
}

unsigned char* _dp_data(data_parser_t* dp) {
	return &(dp->buffer->data[dp->pos]);
}

void _dp_advance(data_parser_t* dp, size_t size) {
	dp->pos += size;
}

int db_bytes(data_parser_t* dp, unsigned char** bs, size_t s) {
	if (!_dp_is_in_buffer_range(dp, s)) {
		return 0;
	}

	*bs = _dp_data(dp);
	_dp_advance(dp, s);

	return 1;
}

int dp_uint8(data_parser_t* dp, uint8_t* u8) {
	unsigned char* bs = NULL;
	if (!db_bytes(dp, &bs, sizeof(uint8_t))) {
		return 0;
	}
	*u8 = *(uint8_t*)bs;
	return 1;
}

int dp_uint16(data_parser_t* dp, uint16_t* u16) {
	unsigned char* bs = NULL;
	if (!db_bytes(dp, &bs, sizeof(uint16_t))) {
		return 0;
	}
	*u16 = *(uint16_t*)bs;
	return 1;

	return db_bytes(dp, (unsigned char**)&u16, sizeof(uint16_t));
}

int dp_uint32(data_parser_t* dp, uint32_t* u32) {
	unsigned char* bs = NULL;
	if (!db_bytes(dp, &bs, sizeof(uint32_t))) {
		return 0;
	}
	*u32 = *(uint32_t*)bs;
	return 1;
}

int dp_string(data_parser_t* dp, char** str) {
	size_t len = 0;
	size_t max_len = 0;
	max_len = dp->buffer->size - dp->pos;
	len = strnlen(_dp_data(dp), max_len);
	if (len == max_len) {
		return 0;
	}

	*str = (char*)_dp_data(dp);
	_dp_advance(dp, len + 1);

	return 1;
}