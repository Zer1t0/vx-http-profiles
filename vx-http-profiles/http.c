
#include "http.h"

#include <winhttp.h>
#include <stdio.h>
#include "alloc.h"
#include <assert.h>
#include "basic_utils.h"

http_request_options_t* http_request_options_new(
	wchar_t* method,
	wchar_t* uri,
	wstring_list_t* headers,
	buffer_t* body
) {
	http_request_options_t* hro = MALLOC(sizeof(http_request_options_t));
	assert(hro);

	hro->method = method;
	hro->uri = uri;
	hro->headers = headers;
	hro->body = body;

	return hro;
}

http_response_t* http_response_new(wstring_list_t* headers, buffer_t* body) {
	http_response_t* hr = MALLOC(sizeof(http_response_t));
	assert(hr);

	hr->headers = headers;
	hr->body = body;

	return hr;
}

void http_response_destroy(http_response_t** hr) {
	buffer_destroy(&(*hr)->body);
	wstring_list_destroy(&(*hr)->headers);
	DESTROY(*hr);
}

void http_request_options_destroy(http_request_options_t** hro) {
	DESTROY((*hro)->method);
	DESTROY((*hro)->uri);
	wstring_list_destroy(&(*hro)->headers);
	buffer_destroy(&(*hro)->body);

	DESTROY(*hro);
}

DWORD HttpQueryStatus(HANDLE hRequest) {
	DWORD StatusCode = 0;
	DWORD StatusSize = sizeof(DWORD);

	if (WinHttpQueryHeaders(
		hRequest,
		WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
		WINHTTP_HEADER_NAME_BY_INDEX,
		&StatusCode, &StatusSize, WINHTTP_NO_HEADER_INDEX
	)
		)
	{
		return StatusCode;
	}

	return 0;
}

buffer_t* read_response_body(HANDLE request) {
	unsigned char* tmp[1024] = { 0 };
	DWORD   read_count = 0;
	BOOL ok = FALSE;
	unsigned char* resp_bytes = NULL;
	size_t  resp_size = 0;
	buffer_t* resp_buffer = NULL;

	while (1) {
		ok = WinHttpReadData(request, tmp, 1024, &read_count);
		if (!ok || read_count == 0) {
			break;
		}

		if (!resp_bytes) {
			resp_bytes = MALLOC(read_count);
		}
		else {
			resp_bytes = RECALLOC(resp_bytes, resp_size + read_count);
		}

		resp_size += read_count;

		memcpy(resp_bytes + (resp_size - read_count), tmp, read_count);
		memset(tmp, 0, 1024);

	}

	if (resp_bytes) {
		resp_buffer = buffer_new(resp_bytes, resp_size);
	}

	return resp_buffer;
}

static wstring_list_t* split_headers(wchar_t* raw_headers) {
	size_t headers_count = 0;
	wchar_t* ptr = NULL;
	size_t header_len = 0;
	wchar_t** headers = NULL;
	size_t i = 0;
	wstring_list_t* header_list = NULL;

	ptr = raw_headers;
	while (1) {
		header_len = wcslen(ptr);
		if (!header_len) {
			break;
		}
		headers_count++;
		ptr += header_len + 1;
	}

	headers = CALLOC(sizeof(wchar_t*) * headers_count);
	assert(headers);

	ptr = raw_headers;
	for (i = 0;; i++) {
		header_len = wcslen(ptr);
		if (!header_len) {
			break;
		}
		headers[i] = copy_wstring(ptr);
		ptr += header_len + 1;
	}

	header_list = wstring_list_new(headers_count, headers);

	return header_list;
}

wstring_list_t* read_response_headers(HANDLE request) {
	BOOL ok = FALSE;
	DWORD headers_size = 0;
	wchar_t* headers_raw = NULL;
	wchar_t* first_header = NULL;
	wstring_list_t* header_list = NULL;

	ok = WinHttpQueryHeaders(
		request,
		WINHTTP_QUERY_RAW_HEADERS,
		WINHTTP_HEADER_NAME_BY_INDEX,
		WINHTTP_NO_OUTPUT_BUFFER,
		&headers_size, 
		WINHTTP_NO_HEADER_INDEX
	);
	if (!(!ok && (GetLastError() == ERROR_INSUFFICIENT_BUFFER))) {
		goto close;
	}
	headers_raw = CALLOC(headers_size);
	assert(headers_raw);

	ok = WinHttpQueryHeaders(request,
		WINHTTP_QUERY_RAW_HEADERS, 
		WINHTTP_HEADER_NAME_BY_INDEX,
		headers_raw, 
		&headers_size, 
		WINHTTP_NO_HEADER_INDEX
	);
	if (!ok) {
		goto close;
	}

	// First string is response code, so jump ahead
	first_header = headers_raw + wcslen(headers_raw) + 1;
	header_list = split_headers(first_header);
	first_header = NULL;

close:
	if (headers_raw) DESTROY(headers_raw);
	return header_list;
}

static http_response_t* read_response(HANDLE request) {
	http_response_t* hr = NULL;
	buffer_t* body_buffer = NULL;
	wstring_list_t* header_list = NULL;

	body_buffer = read_response_body(request);
	if (!body_buffer) {
		goto close;
	}
	header_list = read_response_headers(request);
	if (!header_list) {
		goto close;
	}

	hr = http_response_new(header_list, body_buffer);
	header_list = NULL;
	body_buffer = NULL;

close:
	if (body_buffer) buffer_destroy(&body_buffer);
	if (header_list) wstring_list_destroy(&header_list);
	return hr;
}

http_response_t* http_send(
	LPCWSTR host,
	WORD port,
	http_request_options_t* options
) {
	HANDLE  connection = NULL;
	HANDLE  session = NULL;
	HANDLE  request = NULL;
	DWORD context = 0;
	int ok = 0;
	size_t i = 0;
	wchar_t* header = NULL;
	http_response_t* response = NULL;
	

	session = WinHttpOpen(
		NULL,
		0,
		NULL,
		WINHTTP_NO_PROXY_BYPASS,
		0
	);
	if (!session) {
		printf("WinHttpOpen: Failed => %d\n", GetLastError());
		goto close;
	}

	connection = WinHttpConnect(session, host, port, 0);
	if (!connection) {
		printf("WinHttpConnect: Failed => %d\n", GetLastError());
		goto close;
	}

	request = WinHttpOpenRequest(
		connection,
		options->method, options->uri,
		NULL,
		NULL,
		NULL,
		0
	);
	if (!request) {
		printf("WinHttpOpenRequest: Failed => %d\n", GetLastError());
		goto close;
	}

	for (i = 0; i < options->headers->count; i++) {
		header = options->headers->strings[i];

		WinHttpAddRequestHeaders(request, header, -1, WINHTTP_ADDREQ_FLAG_ADD);
	}

	if (!WinHttpSendRequest(
		request,
		NULL,
		0,
		options->body->data,
		(DWORD)options->body->size,
		(DWORD)options->body->size,
		(DWORD_PTR)NULL
	)) {
		goto close;
	}

	if (!WinHttpReceiveResponse(request, NULL)) {
		goto close;
	}

	if (HttpQueryStatus(request) != HTTP_STATUS_OK) {
		printf("HttpQueryStatus Failed: Is not HTTP_STATUS_OK (200)");
		goto close;
	}

	response = read_response(request);

close:
	if (session) WinHttpCloseHandle(session);
	if (connection) WinHttpCloseHandle(connection);
	if (request) WinHttpCloseHandle(request);

	return response;
}

