
#ifndef BUFFER_H
#define BUFFER_H

#include <stdint.h>

typedef struct _buffer_t {
	unsigned char* data;
	size_t size;
} buffer_t;

buffer_t* buffer_new(unsigned char* data, size_t size);
buffer_t* buffer_new_copy(unsigned char* data, size_t size);
void buffer_destroy(buffer_t** b);
buffer_t* buffer_from_string(char* str);

#endif