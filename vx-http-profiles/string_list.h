#ifndef STRING_LIST_H
#define STRING_LIST_H

#include <windows.h>

typedef struct _wstring_list_t {
	size_t count;
	wchar_t** strings;
} wstring_list_t;


typedef struct _string_list_t {
	size_t count;
	char** strings;
} string_list_t;

string_list_t* string_list_new(size_t count, char** strings);
void string_list_destroy(string_list_t** sl);

wstring_list_t* wstring_list_new(size_t count, wchar_t** strings);
void wstring_list_destroy(wstring_list_t** wl);

void wstring_list_append(wstring_list_t* wl, wchar_t* str);

#endif