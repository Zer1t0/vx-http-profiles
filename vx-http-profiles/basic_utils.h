
#ifndef BASIC_UTILS_H
#define BASIC_UTILS_H

#include <stdint.h>

unsigned char* copy_data(const unsigned char* input, size_t input_size);
char* copy_string(const char* str);
wchar_t* copy_wstring(const wchar_t* wstr);
char* replace_string(const char* str, const char* old_value, const char* new_value);
void concat_string(char** str1, const char* str2);

void append(
	const unsigned char* input1, size_t input1_size,
	const unsigned char* input2, size_t input2_size,
	unsigned char** output, size_t* output_size
);

int randomnize(unsigned char* data, size_t size);

wchar_t* to_unicode(const char* ascii);
char* to_ascii(const wchar_t* unicode);

#endif