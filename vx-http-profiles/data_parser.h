
#ifndef DATA_PARSER_H
#define DATA_PARSER_H

#include "buffer.h"

typedef struct _data_parser_t {
	buffer_t* buffer;
	size_t pos;
} data_parser_t;


void data_parser_destroy(data_parser_t** dp);
data_parser_t* data_parser_from_bytes_copy(unsigned char* data, size_t size);

int db_bytes(data_parser_t* dp, unsigned char** bs, size_t s);

int dp_uint8(data_parser_t* dp, uint8_t* u8);
int dp_uint16(data_parser_t* dp, uint16_t* u16);
int dp_uint32(data_parser_t* dp, uint32_t* u32);
int dp_string(data_parser_t* dp, char** str);

#endif