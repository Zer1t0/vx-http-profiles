
#ifndef ALLOC_H
#define ALLOC_H

#include <windows.h>

#define MALLOC(size) HeapAlloc(GetProcessHeap(), 0, size)
#define CALLOC(size) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, size)
#define REALLOC(ptr, size) HeapReAlloc(GetProcessHeap(), 0, ptr, size) 
#define RECALLOC(ptr, size) HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, ptr, size) 
#define FREE(ptr) HeapFree(GetProcessHeap(), 0, ptr)
#define DESTROY(ptr) { FREE(ptr); ptr = NULL;}

#endif /* ALLOC_H */