
#include "http_pipes.h"
#include "alloc.h"
#include <assert.h>

param_pipeline_t* param_pipeline_new(
	char* name,
	uint8_t use_payload,
	buffer_t* initial_value,
	transforms_list_t* transforms
) {
	param_pipeline_t* pp = CALLOC(sizeof(param_pipeline_t));
	assert(pp);

	pp->name = name;
	pp->use_payload = use_payload;
	pp->initial_value = initial_value;
	pp->transforms = transforms;

	return pp;

}

void param_pipeline_destroy(param_pipeline_t** pp) {
	DESTROY((*pp)->name);
	transforms_list_destroy(&(*pp)->transforms);
	DESTROY(*pp);
}

param_pipeline_list_t* param_pipeline_list_new(size_t count, param_pipeline_t** params_pipes) {
	param_pipeline_list_t* ppl = MALLOC(sizeof(param_pipeline_list_t));
	assert(ppl);

	ppl->count = count;
	ppl->params_pipes = params_pipes;

	return ppl;
}

void param_pipeline_list_destroy(param_pipeline_list_t** ppl) {
	size_t i = 0;

	for (i = 0; i < (*ppl)->count; i++) {
		param_pipeline_destroy(&(*ppl)->params_pipes[i]);
	}
	DESTROY((*ppl)->params_pipes);
	DESTROY(*ppl);
}

uri_pipeline_t* uri_pipeline_new(char* path, param_pipeline_list_t* uri_functions) {
	uri_pipeline_t* up = MALLOC(sizeof(uri_pipeline_t));
	assert(up);

	up->path = path;
	up->uri_functions = uri_functions;

	return up;
}

void uri_pipeline_destroy(uri_pipeline_t** up) {
	param_pipeline_list_destroy(&(*up)->uri_functions);
	DESTROY((*up)->path);
	DESTROY(*up);
}

http_pipeline_t* http_pipeline_new(
	uri_pipeline_t* uri,
	param_pipeline_list_t* url_params,
	param_pipeline_list_t* headers,
	param_pipeline_list_t* cookies,
	param_pipeline_t* body
) {
	http_pipeline_t* hp = MALLOC(sizeof(http_pipeline_t));
	assert(hp);

	hp->uri = uri;
	hp->url_params = url_params;
	hp->headers = headers;
	hp->cookies = cookies;
	hp->body = body;
	return hp;

}

void http_pipeline_destroy(http_pipeline_t** hp) {
	uri_pipeline_destroy(&(*hp)->uri);
	param_pipeline_list_destroy(&(*hp)->url_params);
	param_pipeline_list_destroy(&(*hp)->headers);
	param_pipeline_list_destroy(&(*hp)->cookies);
	param_pipeline_destroy(&(*hp)->body);
	DESTROY(*hp);
}

http_resp_pipeline_t* http_resp_pipeline_new(
	param_pipeline_t* body, 
	param_pipeline_t* header,
	param_pipeline_t* cookie
) {
	http_resp_pipeline_t* hr = MALLOC(sizeof(http_resp_pipeline_t));
	assert(hr);

	hr->body = body;
	hr->header = header;
	hr->cookie = cookie;
	return hr;
}

void http_resp_pipeline_destroy(http_resp_pipeline_t** hr) {
	if ((*hr)->body) {
		param_pipeline_destroy(&(*hr)->body);
	}
	if ((*hr)->header) {
		param_pipeline_destroy(&(*hr)->header);
	}
	if ((*hr)->cookie) {
		param_pipeline_destroy(&(*hr)->cookie);
	}

	DESTROY(*hr);
}

http_pipes_t* http_pipes_new(
	http_pipeline_t* get_request,
	http_pipeline_t* post_request,
	http_resp_pipeline_t* get_response,
	http_resp_pipeline_t* post_response
) {
	http_pipes_t* hpp = MALLOC(sizeof(http_pipes_t));
	assert(hpp);

	hpp->get_request = get_request;
	hpp->post_request = post_request;
	hpp->get_response = get_response;
	hpp->post_response = post_response;
	return hpp;
}

void http_pipes_destroy(http_pipes_t** hpp) {
	http_pipeline_destroy(&(*hpp)->get_request);
	http_pipeline_destroy(&(*hpp)->post_request);
	http_resp_pipeline_destroy(&(*hpp)->get_response);
	http_resp_pipeline_destroy(&(*hpp)->post_response);
	DESTROY(*hpp);
}
