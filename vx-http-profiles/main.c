﻿
#include <stdio.h>

#include "http-config.h"
#include "http.h"
#include "http_pipes_parse.h"
#include "http_pipes_process.h"
#include <assert.h>
#include "alloc.h"

#define METHOD "GET"
#define PAYLOAD "Hello world"

void print_buffer(buffer_t* b) {
	char* str = CALLOC(b->size + 1);
	assert(str);
	memcpy(str, b->data, b->size);
	printf("(%zu): %s\n", b->size, str);
	DESTROY(str);
}

int main() {
	unsigned char raw_http_config[] = HTTP_CONFIG;
	http_pipes_t* hpp = NULL;
	http_request_options_t* hro = NULL;
	http_response_t* response = NULL;
	int ok = -1;
	buffer_t* resp_payload = NULL;

	hpp = parse_http_config(raw_http_config, sizeof(raw_http_config));
	if (!hpp) {
		printf("Error parsing config\n");
		goto close;
	}

	hro = process_http_payload(METHOD, hpp, PAYLOAD, strlen(PAYLOAD));
	if (!hro) {
		printf("Error creating HTTP request\n");
		goto close;
	}

	printf("==== HTTP Request ====\n");
	printf("Uri: %ws\n", hro->uri);

	size_t i = 0;
	for (i = 0; i < hro->headers->count; i++) {
		printf("Header -> %ws\n", hro->headers->strings[i]);
	}

	printf("Body ");
	print_buffer(hro->body);
	printf("=======================\n");

	response = http_send(L"127.0.0.1", 8000, hro);
	if (!response) {
		printf("Unable to get HTTP response\n");
		goto close;
	}

	printf("==== HTTP Response ====\n");

	for (i = 0; i < response->headers->count; i++) {
		printf("Header -> %ws\n", response->headers->strings[i]);
	}

	printf("Body ");
	print_buffer(response->body);
	printf("=======================\n");

	resp_payload = process_http_response(METHOD, response, hpp);
	if (!resp_payload) {
		printf("Unable to get response payload\n");
		goto close;
	}
	printf("Payload ");
	print_buffer(resp_payload);

	ok = 0;
close:
	if (resp_payload) buffer_destroy(&resp_payload);
	if (response) http_response_destroy(&response);
	if (hpp) http_pipes_destroy(&hpp);
	return ok;
}
