
#include "http_pipes_process.h"
#include "alloc.h"
#include "basic_utils.h"
#include <assert.h>

int process_param(
	const param_pipeline_t* pair,
	const unsigned char* payload,
	const size_t payload_size,
	unsigned char** output,
	size_t* output_size
) {
	const unsigned char* initial_value = NULL;
	size_t initial_value_size = 0;

	if (pair->use_payload) {
		initial_value = payload;
		initial_value_size = payload_size;
	}
	else {
		initial_value = pair->initial_value->data;
		initial_value_size = pair->initial_value->size;
	}

	return apply_transforms(
		pair->transforms,
		initial_value,
		initial_value_size,
		output, output_size
	);
}

#define HEADER_SEPARATOR ": "

char* join_header_name_value(const char* name, const char* value) {
	char* result = NULL;
	result = CALLOC(strlen(name) + strlen(HEADER_SEPARATOR) + strlen(value) + 1);
	assert(result);

	strcat(result, name);
	strcat(result, HEADER_SEPARATOR);
	strcat(result, value);

	return result;
}

typedef struct _name_value_t {
	char* name;
	unsigned char* value;
	size_t value_size;
} name_value_t;

name_value_t* name_value_new(char* name, unsigned char* value, size_t value_size) {
	name_value_t* nm = MALLOC(sizeof(name_value_t));
	assert(nm);

	nm->name = name;
	nm->value = value;
	nm->value_size = value_size;
	return nm;
}

void name_value_destroy(name_value_t** nm) {
	DESTROY((*nm)->value);
	DESTROY((*nm)->name);
	DESTROY(*nm);
}

typedef struct _name_value_list_t {
	size_t count;
	name_value_t** pairs;
} name_value_list_t;

name_value_list_t* name_value_list_new(size_t count, name_value_t** pairs) {
	name_value_list_t* nml = MALLOC(sizeof(name_value_list_t));
	assert(nml);

	nml->count = count;
	nml->pairs = pairs;

	return nml;
}

void name_value_list_destroy(name_value_list_t** nml) {
	size_t i = 0;

	for (i = 0; i < (*nml)->count; i++) {
		name_value_destroy(&(*nml)->pairs[i]);
	}

	DESTROY((*nml)->pairs);
	DESTROY(*nml);
}

name_value_list_t* process_params(
	param_pipeline_list_t* params,
	const unsigned char* payload,
	const size_t payload_size
) {
	size_t i = 0;
	name_value_t** pairs = NULL;
	name_value_list_t* list = NULL;
	wchar_t* header = NULL;
	size_t pairs_count = 0;
	unsigned char* value = NULL;
	size_t value_size = 0;

	pairs_count = params->count;
	pairs = CALLOC(sizeof(name_value_t*) * pairs_count);
	assert(pairs);


	for (i = 0; i < pairs_count; i++) {
		if (!process_param(
			params->params_pipes[i],
			payload,
			payload_size,
			&value,
			&value_size
		)) {
			goto close;
		}
		pairs[i] = name_value_new(copy_string(params->params_pipes[i]->name), value, value_size);
	}

	list = name_value_list_new(pairs_count, pairs);
	pairs = NULL;

close:
	if (pairs) {
		for (i = 0; i < pairs_count; i++) {
			if (!pairs[i]) {
				break;
			}
			name_value_destroy(&pairs[i]);
		}
		DESTROY(pairs);
	}
	return list;
}

wstring_list_t* name_value_to_headers(name_value_list_t* nml) {
	size_t i = 0;
	char* header_ascii = NULL;
	wchar_t* header = NULL;
	name_value_t* nm = NULL;
	wchar_t** headers = NULL;
	size_t headers_count = 0;

	headers_count = nml->count;
	headers = CALLOC(sizeof(wchar_t*) * headers_count);
	assert(headers);

	for (i = 0; i < nml->count; i++) {
		nm = nml->pairs[i];
		header_ascii = join_header_name_value(nm->name, nm->value);
		header = to_unicode(header_ascii);
		DESTROY(header_ascii);
		headers[i] = header;
	}

	return wstring_list_new(headers_count, headers);
}

wstring_list_t* process_http_headers(
	param_pipeline_list_t* headers_pipes,
	const unsigned char* payload,
	const size_t payload_size
) {
	name_value_list_t* nml = NULL;
	wstring_list_t* headers_list = NULL;

	nml = process_params(headers_pipes, payload, payload_size);
	if (!nml) {
		goto close;
	}

	headers_list = name_value_to_headers(nml);

close:
	if (nml) name_value_list_destroy(&nml);
	return headers_list;
}

wchar_t* name_value_list_to_uri(const char* base_uri, name_value_list_t* nml) {
	char* uri_ascii = NULL;
	char* tmp = NULL;
	size_t i = 0;
	wchar_t* uri = NULL;

	uri_ascii = copy_string(base_uri);
	for (i = 0; i < nml->count; i++) {
		tmp = replace_string(uri_ascii, nml->pairs[i]->name, nml->pairs[i]->value);
		DESTROY(uri_ascii);
		uri_ascii = tmp;
		tmp = NULL;
	}

	uri = to_unicode(uri_ascii);
	return uri;
}

buffer_t* process_http_body(
	const param_pipeline_t* pair,
	const unsigned char* payload,
	const size_t payload_size
) {
	unsigned char* value = NULL;
	size_t value_size = 0;
	buffer_t* body = NULL;

	if (!process_param(pair, payload, payload_size, &value, &value_size)) {
		goto close;
	}

	body = buffer_new(value, value_size);

close:
	return body;
}

wchar_t* process_http_uri(
	uri_pipeline_t* up,
	const unsigned char* payload,
	const size_t payload_size
) {
	name_value_list_t* nml = NULL;
	wchar_t* uri = NULL;

	nml = process_params(up->uri_functions, payload, payload_size);
	if (!nml) {
		goto close;
	}
	uri = name_value_list_to_uri(up->path, nml);
close:
	if (nml) name_value_list_destroy(&nml);
	return uri;
}

#define COOKIE_NAME_VALUE_SEPARATOR " = "

char* join_cookie_name_value(const char* name, const char* value) {
	char* cookie = CALLOC(
		strlen(name) + strlen(COOKIE_NAME_VALUE_SEPARATOR) + strlen(value) + 1
	);
	assert(cookie);

	strcat(cookie, name);
	strcat(cookie, COOKIE_NAME_VALUE_SEPARATOR);
	strcat(cookie, value);

	return cookie;
}

#define COOKIES_SEPARATOR "; "

wchar_t* name_value_list_to_cookie_header(
	name_value_list_t* nml
) {
	char* cookies = NULL;
	char* cookie = NULL;
	size_t cookies_count = 0;
	size_t i = 0;
	char* cookie_header_ascii = NULL;
	wchar_t* cookie_header = NULL;

	cookies_count = nml->count;
	cookies = CALLOC(1);
	assert(cookies);

	for (i = 0; i < cookies_count; i++) {
		cookie = join_cookie_name_value(nml->pairs[i]->name, nml->pairs[i]->value);
		concat_string(&cookies, cookie);
		concat_string(&cookies, COOKIES_SEPARATOR);
	}

	if (cookies_count > 0) {
		memset(
			cookies + strlen(cookies) - strlen(COOKIES_SEPARATOR),
			0,
			strlen(COOKIES_SEPARATOR)
		);
	}

	cookie_header_ascii = join_header_name_value("Cookie", cookies);
	cookie_header = to_unicode(cookie_header_ascii);

	DESTROY(cookie_header_ascii);
	DESTROY(cookies);

	return cookie_header;
}

wchar_t* process_http_cookies(
	param_pipeline_list_t* cookies_pipes,
	const unsigned char* payload,
	size_t payload_size
) {
	name_value_list_t* nml = NULL;
	char* cookies = NULL;
	wchar_t* cookie_header = NULL;

	nml = process_params(cookies_pipes, payload, payload_size);
	if (!nml) {
		goto close;
	}
	if (nml->count) {
		cookie_header = name_value_list_to_cookie_header(nml);
	}
	else {
		cookie_header = CALLOC(sizeof(wchar_t));
		assert(cookie_header);
	}

close:
	if (nml) name_value_list_destroy(&nml);
	return cookie_header;
}

#define URL_PARAMS_NAME_VALUE_SEPARATOR "="

char* join_url_param_name_value(const char* name, const char* value) {
	char* up = CALLOC(
		strlen(name) + strlen(URL_PARAMS_NAME_VALUE_SEPARATOR) + strlen(value) + 1
	);
	assert(up);

	strcat(up, name);
	strcat(up, URL_PARAMS_NAME_VALUE_SEPARATOR);
	strcat(up, value);

	return up;
}

#define URL_PARAMS_SEPARATOR "&"

wchar_t* name_value_list_to_url_params(name_value_list_t* nml) {
	char* params = NULL;
	char* param = NULL;
	size_t params_count = 0;
	size_t i = 0;
	wchar_t* params_unicode = NULL;

	params_count = nml->count;
	params = CALLOC(1);
	assert(params);

	for (i = 0; i < params_count; i++) {
		param = join_url_param_name_value(nml->pairs[i]->name, nml->pairs[i]->value);
		concat_string(&params, param);
		concat_string(&params, URL_PARAMS_SEPARATOR);
	}

	if (params_count > 0) {
		memset(
			params + strlen(params) - strlen(URL_PARAMS_SEPARATOR),
			0,
			strlen(URL_PARAMS_SEPARATOR)
		);
	}

	params_unicode = to_unicode(params);
	DESTROY(params);

	return params_unicode;
}

void* process_http_url_params(
	param_pipeline_list_t* url_params,
	const unsigned char* payload,
	size_t payload_size
) {
	name_value_list_t* nml = NULL;
	wchar_t* params = NULL;

	nml = process_params(url_params, payload, payload_size);
	if (!nml) {
		goto close;
	}
	if (nml->count) {
		params = name_value_list_to_url_params(nml);
	}
	else {
		params = CALLOC(sizeof(wchar_t));
		assert(params);
	}

close:
	if (nml) name_value_list_destroy(&nml);
	return params;
}

#define URL_PATH_PARAMS_SEPARATOR L"?"

wchar_t* join_url_path_and_params(const wchar_t* path, const wchar_t* params) {
	wchar_t* url = CALLOC(
		(wcslen(path) + wcslen(URL_PATH_PARAMS_SEPARATOR) + wcslen(params) + 1)
		* sizeof(wchar_t)
	);
	assert(url);

	wcscat(url, path);
	wcscat(url, URL_PATH_PARAMS_SEPARATOR);
	wcscat(url, params);

	return url;
}

wchar_t* process_http_url(
	uri_pipeline_t* up,
	param_pipeline_list_t* url_params,
	const unsigned char* payload,
	size_t payload_size
) {
	wchar_t* uri = NULL;
	wchar_t* url = NULL;
	wchar_t* params = NULL;

	uri = process_http_uri(up, payload, payload_size);
	if (!uri) {
		goto close;
	}

	params = process_http_url_params(url_params, payload, payload_size);
	if (!params) {
		goto close;
	}

	if (wcslen(params) != 0) {
		url = join_url_path_and_params(uri, params);
	}
	else {
		url = uri;
		uri = NULL;
	}

close:
	if (uri) DESTROY(uri);
	if (params) DESTROY(params);
	return url;
}

http_request_options_t* process_http_payload(
	const char* method,
	http_pipes_t* hpp,
	const unsigned char* payload,
	const size_t payload_size
) {
	wstring_list_t* url_params = NULL;
	wstring_list_t* headers = NULL;
	http_request_options_t* request = NULL;
	wchar_t* cookie_header = NULL;
	buffer_t* body = NULL;
	wchar_t* url = NULL;
	http_pipeline_t* hp = NULL;

	if (strcmp(method, "GET") == 0) {
		hp = hpp->get_request;
	}
	else if (strcmp(method, "POST") == 0) {
		hp = hpp->post_request;
	}
	else {
		goto close;
	}

	url = process_http_url(hp->uri, hp->url_params, payload, payload_size);
	if (!url) {
		goto close;
	}

	headers = process_http_headers(hp->headers, payload, payload_size);
	if (!headers) {
		goto close;
	}
	cookie_header = process_http_cookies(hp->cookies, payload, payload_size);
	if (!cookie_header) {
		goto close;
	}

	if (wcslen(cookie_header) != 0) {
		wstring_list_append(headers, cookie_header);
	}
	else {
		DESTROY(cookie_header);
	}

	body = process_http_body(hp->body, payload, payload_size);
	if (!body) {
		goto close;
	}

	request = http_request_options_new(to_unicode(method), url, headers, body);
	url = NULL;
	headers = NULL;
	body = NULL;

close:
	if (url) DESTROY(url);
	if (headers) wstring_list_destroy(&headers);
	if (body) buffer_destroy(&body);

	return request;
}


buffer_t* process_http_response_body(
	param_pipeline_t* body_resp,
	buffer_t* body
) {
	unsigned char* value = NULL;
	size_t value_size = 0;
	buffer_t* payload = NULL;

	if (!process_param(body_resp, body->data, body->size, &value, &value_size)) {
		goto close;
	}

	payload = buffer_new(value, value_size);
	value = NULL;

close:
	if (value) DESTROY(value);
	return payload;
}

name_value_t* divide_header(const wchar_t* header_uni) {
	char* header = NULL;
	name_value_t* nm = NULL;
	char* sep_ptr = NULL;
	size_t sep_len = 0;
	char* name = NULL;
	size_t name_len = 0;
	char* value = NULL;
	size_t value_len = 0;
	char* value_start_ptr = NULL;

	header = to_ascii(header_uni);
	if (!header) {
		goto close;
	}

	sep_ptr = strstr(header, HEADER_SEPARATOR);
	if (!sep_ptr) {
		goto close;
	}

	name_len = sep_ptr - header;
	name = CALLOC(name_len + 1);
	assert(name);
	memcpy(name, header, name_len);

	sep_len = strlen(HEADER_SEPARATOR);
	value_start_ptr = sep_ptr + sep_len;
	value_len = strlen(header) - (name_len + sep_len);
	
	value = CALLOC(value_len + 1);
	assert(value);
	memcpy(value, value_start_ptr, value_len);

	nm = name_value_new(name, value, strlen(value));
	name = NULL;
	value = NULL;

close:
	if (value) DESTROY(value);
	if (name) DESTROY(name);
	if (header) DESTROY(header);
	return nm;
}


name_value_list_t* divide_headers(wstring_list_t* wstr_list) {
	size_t i = 0;
	size_t headers_count = 0;
	
	name_value_list_t* nml = NULL;
	name_value_t** pairs = NULL;
	name_value_t* pair = NULL;

	headers_count = wstr_list->count;
	pairs = CALLOC(sizeof(name_value_t*) * headers_count);
	assert(pairs);

	for (i = 0; i < headers_count; i++) {
		pair = divide_header(wstr_list->strings[i]);
		if (!pair) {
			goto close;
		}
		pairs[i] = pair;
	}

	nml = name_value_list_new(headers_count, pairs);
	pairs = NULL;

close:
	if (pairs) {
		for (i = 0; i < headers_count; i++) {
			if (!pairs[i]) {
				break;
			}
			DESTROY(pairs[i]);
		}
		DESTROY(pairs);
	}
	return nml;
}

buffer_t* process_http_response_header(
	param_pipeline_t* header_pipe, 
	name_value_list_t* headers
) {
	size_t i = 0;
	unsigned char* value = NULL;
	size_t value_size = 0;
	buffer_t* payload = NULL;

	for (i = 0; i < headers->count; i++) {
		if (strcmp(header_pipe->name, headers->pairs[i]->name) == 0) {
			if (!process_param(
				header_pipe, 
				headers->pairs[i]->value, 
				headers->pairs[i]->value_size, 
				&value, 
				&value_size
			)) {
				goto close;
			}

			payload = buffer_new(value, value_size);
			value = NULL;
			break;
		}
	}

close:
	if (value) DESTROY(value);
	return payload;
}

buffer_t* process_http_response_cookie(
	param_pipeline_t* cookie_pipe,
	name_value_list_t* cookies
) {
	return process_http_response_header(cookie_pipe, cookies);
}

name_value_t* divide_cookie(const char* cookie) {
	char* name = NULL;
	char* value = NULL;
	const char* sep_ptr = NULL;
	name_value_t* nm = NULL;
	size_t name_len = 0;
	size_t value_len = 0;
	const char* value_start_ptr = NULL;
	const char* value_end_ptr = NULL;
	size_t cookie_len = 0;

	sep_ptr = strstr(cookie, "=");
	if (!sep_ptr) {
		goto close;
	}

	name_len = sep_ptr - cookie;
	name = CALLOC(name_len + 1);
	assert(name);
	memcpy(name, cookie, name_len);

	cookie_len = strlen(cookie);
	value_start_ptr = sep_ptr + 1;
	if (*value_start_ptr == '"') {
		value_start_ptr++;
	}
	value_end_ptr = cookie + cookie_len - 1;
	if (*value_end_ptr == '"') {
		value_end_ptr--;
	}

	if (value_start_ptr > value_end_ptr) {
		goto close;
	}

	value_len = value_end_ptr - value_start_ptr + 1;
	value = CALLOC(value_len + 1);
	assert(value);
	memcpy(value, value_start_ptr, value_len);

	nm = name_value_new(name, value, strlen(value));
	name = NULL;
	value = NULL;

close:
	if (name) DESTROY(name);
	if (value) DESTROY(value);
	return nm;
}

name_value_t* extract_cookie_from_header(const char* value) {
	char* sep_ptr = NULL;
	name_value_t* nm = NULL;
	char* start_ptr = NULL;
	char* local_value = NULL;

	local_value = copy_string(value);
	start_ptr = local_value;
	while (*start_ptr == ' ') start_ptr++;

	sep_ptr = strstr(start_ptr, ";");
	if (sep_ptr) {
		*sep_ptr = 0;
	}
	nm = divide_cookie(start_ptr);

	DESTROY(local_value);

	return nm;
}

name_value_list_t* extract_cookies_from_headers(name_value_list_t* headers) {
	size_t i = 0;
	size_t j = 0;
	size_t cookies_count = 0;
	name_value_t** cookies = NULL;
	name_value_list_t* nml = NULL;

	for (i = 0; i < headers->count; i++) {
		if (strcmp(headers->pairs[i]->name, "Set-Cookie") == 0) {
			cookies_count++;
		}
	}

	cookies = CALLOC(sizeof(name_value_t*) * cookies_count);
	assert(cookies);

	j = 0;
	for (i = 0; i < headers->count; i++) {
		if (strcmp(headers->pairs[i]->name, "Set-Cookie") == 0) {
			cookies[j] = extract_cookie_from_header(headers->pairs[i]->value);
			if (!cookies[j]) {
				goto close;
			}
			j++;
		}
	}

	nml = name_value_list_new(cookies_count, cookies);
	cookies = NULL;

close:
	if (cookies) {
		for (j = 0; j < cookies_count; j++) {
			if (!cookies[j]) {
				break;
			}
			DESTROY(cookies[j]);
		}
		DESTROY(cookies);
	}
	return nml;
}

buffer_t* process_http_response(
	char* method,
	http_response_t* response,
	http_pipes_t* hpp
) {
	http_resp_pipeline_t* hp = NULL;
	buffer_t* payload = NULL;
	name_value_list_t* headers = NULL;
	name_value_list_t* cookies = NULL;

	if (strcmp(method, "GET") == 0) {
		hp = hpp->get_response;
	}
	else if (strcmp(method, "POST") == 0) {
		hp = hpp->post_response;
	}
	else {
		goto close;
	}

	if (hp->body) {
		payload = process_http_response_body(hp->body, response->body);
		goto close;
	}

	headers = divide_headers(response->headers);
	if (!headers) {
		goto close;
	}

	if (hp->header) {
		payload = process_http_response_header(hp->header, headers);
		goto close;
	}

	cookies = extract_cookies_from_headers(headers);
	if (!cookies) {
		goto close;
	}

	if (hp->cookie) {
		payload = process_http_response_cookie(hp->cookie, cookies);
		goto close;
	}

close:
	if (cookies) name_value_list_destroy(&cookies);
	if (headers) name_value_list_destroy(&headers);
	return payload;
}