
#ifndef HTTP_H
#define HTTP_H

#include <windows.h>
#include "buffer.h"
#include "string_list.h"

typedef struct _http_request_options_t {
	wchar_t* method;
	wchar_t* uri;
	wstring_list_t* headers;
	buffer_t* body;
} http_request_options_t;

http_request_options_t* http_request_options_new(
	wchar_t* method, 
	wchar_t* uri, 
	wstring_list_t* headers, 
	buffer_t* body
);

void http_request_options_destroy(http_request_options_t** hro);

typedef struct _http_response_t {
	wstring_list_t* headers;
	buffer_t* body;
} http_response_t;

http_response_t* http_response_new(wstring_list_t* headers, buffer_t* body);
void http_response_destroy(http_response_t** hr);

http_response_t* http_send(
	LPCWSTR host,
	WORD port,
	http_request_options_t* options
);

#endif