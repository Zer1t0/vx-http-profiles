
#ifndef TRANSFORMS_H
#define TRANSFORMS_H

#include <stdint.h>

#define APPEND_CODE 1
#define RANDOM_ALPHA_CODE 2
#define BASE64_CODE 3
#define CHOOSE_RANDOM_CODE 4
#define PREPEND_CODE 5

typedef int (*transform_f)(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
	);

typedef struct _transform_t {
	uint16_t type;
	uint8_t is_reverse;
	void* parameters;
	transform_f function;
} transform_t;

transform_t* transform_new(uint16_t type, uint8_t is_reverse, void* parameters, transform_f function);
void transform_destroy(transform_t** t);

typedef struct _transforms_list_t {
	size_t count;
	transform_t** transforms;
} transforms_list_t;

transforms_list_t* transforms_list_new(size_t count, transform_t** transforms);
void transforms_list_destroy(transforms_list_t** tl);

int transform_append(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int reverse_transform_append(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int transform_encode_base64(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int reverse_transform_encode_base64(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int transform_prepend(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int reverse_transform_prepend(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int transform_random_alpha(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int reverse_transform_random_alpha(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

int transform_choose_random(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
);

void destroy_append_params(void** params);
void destroy_prepend_params(void** params);
void destroy_choose_random_params(void** params);
void destroy_base64_params(void** params);
void destroy_random_alpha_params(void** params);

int apply_transforms(
	transforms_list_t* transforms,
	const unsigned char* initial_data,
	size_t initial_data_size,
	unsigned char** output,
	size_t* output_size
);

#endif