#ifndef PRINT_H
#define PRINT_H

#include <stdio.h>

#define PRINTF_DEBUG( f, ... )    { printf( "[DEBUG::%s::%d] " f, __FUNCTION__, __LINE__, __VA_ARGS__ ); }

#endif