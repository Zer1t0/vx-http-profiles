#ifndef HTTP_PIPES_H
#define HTTP_PIPES_H

#include <stdint.h>
#include "buffer.h"
#include "transforms.h"

typedef struct _param_pipeline_t {
	char* name;
	uint8_t use_payload;
	buffer_t* initial_value;
	transforms_list_t* transforms;
} param_pipeline_t;

param_pipeline_t* param_pipeline_new(
	char* name,
	uint8_t use_payload,
	buffer_t* initial_value,
	transforms_list_t* transforms
);

void param_pipeline_destroy(param_pipeline_t** pp);

typedef struct _param_pipeline_list_t {
	size_t count;
	param_pipeline_t** params_pipes;
} param_pipeline_list_t;

param_pipeline_list_t* param_pipeline_list_new(size_t count, param_pipeline_t** params_pipes);
void param_pipeline_list_destroy(param_pipeline_list_t** ppl);

typedef struct _uri_pipeline_t {
	char* path;
	param_pipeline_list_t* uri_functions;
} uri_pipeline_t;

uri_pipeline_t* uri_pipeline_new(char* path, param_pipeline_list_t* uri_functions);
void uri_pipeline_destroy(uri_pipeline_t** up);

typedef struct _http_pipeline_t {
	uri_pipeline_t* uri;
	param_pipeline_list_t* url_params;
	param_pipeline_list_t* headers;
	param_pipeline_list_t* cookies;
	param_pipeline_t* body;
} http_pipeline_t;

http_pipeline_t* http_pipeline_new(
	uri_pipeline_t* uri,
	param_pipeline_list_t* url_params,
	param_pipeline_list_t* headers,
	param_pipeline_list_t* cookies,
	param_pipeline_t* body
);
void http_pipeline_destroy(http_pipeline_t** hp);

typedef struct _http_resp_pipeline_t {
	param_pipeline_t* body;
	param_pipeline_t* header;
	param_pipeline_t* cookie;
} http_resp_pipeline_t;

http_resp_pipeline_t* http_resp_pipeline_new(
	param_pipeline_t* body,
	param_pipeline_t* header,
	param_pipeline_t* cookie
);
void http_resp_pipeline_destroy(http_resp_pipeline_t** hr);

typedef struct _http_pipes_t {
	http_pipeline_t* get_request;
	http_pipeline_t* post_request;
	http_resp_pipeline_t* get_response;
	http_resp_pipeline_t* post_response;
} http_pipes_t;

http_pipes_t* http_pipes_new(
	http_pipeline_t* get_request, 
	http_pipeline_t* post_request,
	http_resp_pipeline_t* get_response,
	http_resp_pipeline_t* post_response
);
void http_pipes_destroy(http_pipes_t** hpp);

#endif