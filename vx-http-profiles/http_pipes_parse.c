
#include "http_pipes_parse.h"
#include "data_parser.h"
#include "string_list.h"
#include "basic_utils.h"
#include "alloc.h"
#include <assert.h>

int parse_random_alpha_params(data_parser_t* dp, void** params) {
	uint32_t chars_count = 0;

	if (!dp_uint32(dp, &chars_count)) {
		return 0;
	}
	*params = (void*)(unsigned long long)chars_count;

	return 1;
}

int parse_reverse_random_alpha_params(data_parser_t* dp, void** params) {
	return parse_random_alpha_params(dp, params);
}

int parse_append_params(data_parser_t* dp, void** params) {
	char* str = NULL;
	int ok = 0;

	if (!dp_string(dp, &str)) {
		goto close;
	}
	*params = copy_string(str);
	str = NULL;

	ok = 1;
close:
	return ok;
}

int parse_reverse_append_params(data_parser_t* dp, void** params) {
	return parse_reverse_random_alpha_params(dp, params);
}

int parse_prepend_params(data_parser_t* dp, void** params) {
	return parse_append_params(dp, params);
}

int parse_reverse_prepend_params(data_parser_t* dp, void** params) {
	return parse_reverse_random_alpha_params(dp, params);
}


int parse_choose_random_params(data_parser_t* dp, void** params) {
	uint8_t count = 0;
	uint8_t i = 0;
	char** strings = NULL;
	char* str = NULL;
	int ok = 0;

	if (!dp_uint8(dp, &count)) {
		goto close;
	}

	strings = CALLOC(sizeof(char*) * count);
	assert(strings);

	for (i = 0; i < count; i++) {
		if (!dp_string(dp, &str)) {
			goto close;
		}
		strings[i] = copy_string(str);
		str = NULL;
	}

	*params = string_list_new(count, strings);
	strings = NULL;

	ok = 1;
close:
	if (strings) {
		for (i = 0; i < count; i++) {
			if (!strings[i]) {
				break;
			}
			DESTROY(strings[i]);
		}
		DESTROY(strings);
	}

	return ok;
}


int parse_base64_params(data_parser_t* dp, void** params) {
	*params = NULL;
	return 1;
}

int parse_reverse_base64_params(data_parser_t* dp, void** params) {
	*params = NULL;
	return 1;
}

int parse_transform(data_parser_t* dp, transform_t** transform) {
	uint16_t type = 0;
	transform_f function = NULL;
	void* params = NULL;
	int ok = 0;

	if (!dp_uint16(dp, &type)) {
		goto close;
	}

	switch (type) {
	case APPEND_CODE:
		function = transform_append;
		if (!parse_append_params(dp, &params)) {
			goto close;
		}
		break;
	case RANDOM_ALPHA_CODE:
		function = transform_random_alpha;
		if (!parse_random_alpha_params(dp, &params)) {
			goto close;
		}
		break;
	case BASE64_CODE:
		function = transform_encode_base64;
		if (!parse_base64_params(dp, &params)) {
			goto close;
		}
		break;
	case CHOOSE_RANDOM_CODE:
		function = transform_choose_random;
		if (!parse_choose_random_params(dp, &params)) {
			goto close;
		}
		break;
	case PREPEND_CODE:
		function = transform_prepend;
		if (!parse_prepend_params(dp, &params)) {
			goto close;
		}
		break;
	default:
		break;
	}

	assert(function);

	*transform = transform_new(type, 0, params, function);
	params = NULL;
	function = NULL;
	ok = 1;

close:
	return ok;
}

int parse_transforms(
	data_parser_t* dp,
	transforms_list_t** transforms_list
) {
	uint8_t count = 0;
	uint8_t i = 0;
	transform_t** transforms = NULL;
	int ok = 0;

	if (!dp_uint8(dp, &count)) {
		goto close;
	}

	transforms = CALLOC(sizeof(transform_t*) * count);
	assert(transforms);

	for (i = 0; i < count; i++) {
		if (!parse_transform(dp, &transforms[i])) {
			goto close;
		}
	}

	*transforms_list = transforms_list_new(count, transforms);
	transforms = NULL;

	ok = 1;
close:

	if (transforms) {
		for (i = 0; i < count; i++) {
			if (!transforms[i]) {
				break;
			}
			transform_destroy(&transforms[i]);
		}
		DESTROY(transforms);
	}

	return ok;
}


int parse_reverse_transform(data_parser_t* dp, transform_t** transform) {
	uint16_t type = 0;
	transform_f function = NULL;
	void* params = NULL;
	int ok = 0;

	if (!dp_uint16(dp, &type)) {
		goto close;
	}

	switch (type) {
	case APPEND_CODE:
		function = reverse_transform_append;
		if (!parse_reverse_append_params(dp, &params)) {
			goto close;
		}
		break;
	case RANDOM_ALPHA_CODE:
		function = reverse_transform_random_alpha;
		if (!parse_reverse_random_alpha_params(dp, &params)) {
			goto close;
		}
		break;
	case BASE64_CODE:
		function = reverse_transform_encode_base64;
		if (!parse_reverse_base64_params(dp, &params)) {
			goto close;
		}
		break;
	case CHOOSE_RANDOM_CODE:
		//unable to reverse this one
		assert(0);
		break;
	case PREPEND_CODE:
		function = reverse_transform_prepend;
		if (!parse_reverse_prepend_params(dp, &params)) {
			goto close;
		}
		break;
	default:
		break;
	}

	assert(function);

	*transform = transform_new(type, 1, params, function);
	params = NULL;
	function = NULL;
	ok = 1;

close:
	return ok;
}


int parse_reverse_transforms(
	data_parser_t* dp,
	transforms_list_t** transforms_list
) {
	uint8_t count = 0;
	uint8_t i = 0;
	transform_t** transforms = NULL;
	int ok = 0;

	if (!dp_uint8(dp, &count)) {
		goto close;
	}

	transforms = CALLOC(sizeof(transform_t*) * count);
	assert(transforms);

	for (i = 0; i < count; i++) {
		if (!parse_reverse_transform(dp, &transforms[i])) {
			goto close;
		}
	}

	*transforms_list = transforms_list_new(count, transforms);
	transforms = NULL;

	ok = 1;
close:

	if (transforms) {
		for (i = 0; i < count; i++) {
			if (!transforms[i]) {
				break;
			}
			transform_destroy(&transforms[i]);
		}
		DESTROY(transforms);
	}

	return ok;
}

int parse_body(data_parser_t* dp, param_pipeline_t** body) {
	char* name = NULL;
	char* value_str = NULL;
	transforms_list_t* transforms = NULL;
	size_t transforms_count = 0;
	uint8_t use_payload = 0;
	int ok = 0;

	if (!dp_uint8(dp, &use_payload)) {
		goto close;
	}

	if (!parse_transforms(dp, &transforms)) {
		goto close;
	}

	name = CALLOC(sizeof(char));

	*body = param_pipeline_new(
		name,
		use_payload,
		buffer_new(MALLOC(0), 0),
		transforms
	);


	ok = 1;
close:

	return ok;
}

int parse_param(data_parser_t* dp, param_pipeline_t** param) {
	char* name = NULL;
	char* value_str = NULL;
	transforms_list_t* transforms = NULL;
	size_t transforms_count = 0;
	uint8_t use_payload = 0;
	int ok = 0;

	if (!dp_string(dp, &name)) {
		goto close;
	}

	if (!dp_uint8(dp, &use_payload)) {
		goto close;
	}

	if (!dp_string(dp, &value_str)) {
		goto close;
	}

	if (!parse_transforms(dp, &transforms)) {
		goto close;
	}

	*param = param_pipeline_new(
		copy_string(name),
		use_payload,
		buffer_from_string(copy_string(value_str)),
		transforms
	);


	ok = 1;
close:

	return ok;
}

int parse_params(
	data_parser_t* dp,
	param_pipeline_list_t** param_list
) {
	uint16_t count = 0;
	uint16_t i = 0;
	param_pipeline_t** local_params = NULL;
	int ok = 0;

	if (!dp_uint16(dp, &count)) {
		goto close;
	}

	local_params = CALLOC(sizeof(param_pipeline_t*) * count);
	assert(local_params);

	for (i = 0; i < count; i++) {
		if (!parse_param(dp, &local_params[i])) {
			goto close;
		}
	}
	*param_list = param_pipeline_list_new(count, local_params);
	local_params = NULL;

	ok = 1;
close:
	if (local_params) {
		for (i = 0; i < count; i++) {
			if (!local_params[i]) {
				break;
			}
			param_pipeline_destroy(&local_params[i]);
		}
		DESTROY(local_params);
	}


	return ok;
}

int parse_uri(data_parser_t* dp, uri_pipeline_t** uri) {
	char* path = NULL;
	param_pipeline_list_t* uri_functions = NULL;
	int ok = 0;

	if (!dp_string(dp, &path)) {
		goto close;
	}

	if (!parse_params(dp, &uri_functions)) {
		goto close;
	}

	*uri = uri_pipeline_new(copy_string(path), uri_functions);
	uri_functions = NULL;

	ok = 1;
close:
	if (uri_functions) param_pipeline_list_destroy(&uri_functions);

	return ok;
}

int parse_http_pipeline(data_parser_t* dp, http_pipeline_t** hp) {
	uri_pipeline_t* uri = NULL;
	param_pipeline_list_t* url_params = NULL;
	param_pipeline_list_t* headers = NULL;
	param_pipeline_list_t* cookies = NULL;
	param_pipeline_t* body = NULL;
	int ok = 0;

	if (!parse_uri(dp, &uri)) {
		goto close;
	}
	if (!parse_params(dp, &url_params)) {
		goto close;
	}
	if (!parse_params(dp, &headers)) {
		goto close;
	}
	if (!parse_params(dp, &cookies)) {
		goto close;
	}
	if (!parse_body(dp, &body)) {
		goto close;
	}

	*hp = http_pipeline_new(uri, url_params, headers, cookies, body);
	uri = NULL;
	url_params = NULL;
	headers = NULL;
	cookies = NULL;
	body = NULL;

	ok = 1;
close:
	if (uri) uri_pipeline_destroy(&uri);
	if (url_params) param_pipeline_list_destroy(&url_params);
	if (headers) param_pipeline_list_destroy(&headers);
	if (cookies) param_pipeline_list_destroy(&cookies);
	if (body) param_pipeline_destroy(&body);

	return ok;
}

int parse_response_body(data_parser_t* dp, param_pipeline_t** body) {
	transforms_list_t* transforms = NULL;
	int ok = 0;

	if (!parse_reverse_transforms(dp, &transforms)) {
		goto close;
	}
	*body = param_pipeline_new(
		CALLOC(1),
		1,
		buffer_new(CALLOC(1), 0),
		transforms
	);
	transforms = NULL;

	ok = 1;
close:
	if (transforms) transforms_list_destroy(&transforms);
	return ok;
}

int parse_response_header(data_parser_t* dp, param_pipeline_t** header) {
	transforms_list_t* transforms = NULL;
	char* name = NULL;
	int ok = 0;

	if (!dp_string(dp, &name)) {
		goto close;
	}

	if (!parse_reverse_transforms(dp, &transforms)) {
		goto close;
	}
	*header = param_pipeline_new(
		copy_string(name),
		1,
		buffer_new(CALLOC(1), 0),
		transforms
	);
	name = NULL;
	transforms = NULL;

	ok = 1;
close:
	if (name) DESTROY(name);
	if (transforms) transforms_list_destroy(&transforms);
	return ok;
}

int parse_response_cookie(data_parser_t* dp, param_pipeline_t** cookie) {
	return parse_response_header(dp, cookie);
}

int parse_http_response_pipeline(data_parser_t* dp, http_resp_pipeline_t** hr) {
	uint8_t is_payload_in_body = 0;
	uint8_t is_payload_in_header = 0;
	uint8_t is_payload_in_cookie = 0;
	param_pipeline_t* body = NULL;
	param_pipeline_t* header = NULL;
	param_pipeline_t* cookie = NULL;
	int ok = 0;

	if (!dp_uint8(dp, &is_payload_in_body)) {
		goto close;
	}
	if (is_payload_in_body) {
		if (!parse_response_body(dp, &body)) {
			goto close;
		}
	}

	if (!dp_uint8(dp, &is_payload_in_header)) {
		goto close;
	}
	if (is_payload_in_header) {
		if (!parse_response_header(dp, &header)) {
			goto close;
		}
	}

	if (!dp_uint8(dp, &is_payload_in_cookie)) {
		goto close;
	}
	if (is_payload_in_cookie) {
		if (!parse_response_cookie(dp, &cookie)) {
			goto close;
		}
	}

	*hr = http_resp_pipeline_new(body, header, cookie);
	body = NULL;
	header = NULL;
	cookie = NULL;

	ok = 1;
close:
	if (body) param_pipeline_destroy(&body);
	if (header) param_pipeline_destroy(&header);
	if (cookie) param_pipeline_destroy(&cookie);

	return ok;
}

int parse_http_pipes(data_parser_t* dp, http_pipes_t** hpp) {
	int ok = 0;
	http_pipeline_t* get_request = NULL;
	http_pipeline_t* post_request = NULL;
	http_resp_pipeline_t* get_resp = NULL;
	http_resp_pipeline_t* post_resp = NULL;

	if (!parse_http_pipeline(dp, &get_request)) {
		goto close;
	}

	if (!parse_http_pipeline(dp, &post_request)) {
		goto close;
	}

	if (!parse_http_response_pipeline(dp, &get_resp)) {
		goto close;
	}

	if (!parse_http_response_pipeline(dp, &post_resp)) {
		goto close;
	}

	*hpp = http_pipes_new(get_request, post_request, get_resp, post_resp);
	get_request = NULL;
	post_request = NULL;
	get_resp = NULL;
	post_resp = NULL;

close:
	if (get_request) http_pipeline_destroy(&get_request);
	if (post_request) http_pipeline_destroy(&post_request);
	if (get_resp) http_resp_pipeline_destroy(&get_resp);
	if (post_resp) http_resp_pipeline_destroy(&post_resp);

	return ok;
}

http_pipes_t* parse_http_config(unsigned char* bs, size_t size) {
	http_pipes_t* hpp = NULL;
	data_parser_t* dp = NULL;

	dp = data_parser_from_bytes_copy(bs, size);
	parse_http_pipes(dp, &hpp);

	data_parser_destroy(&dp);
	return hpp;
}