#ifndef HTTP_PIPES_PARSE_H
#define HTTP_PIPES_PARSE_H

#include "http_pipes.h"

http_pipes_t* parse_http_config(unsigned char* bs, size_t size);

#endif