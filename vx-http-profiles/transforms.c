
#include "transforms.h"

#include "basic_utils.h"
#include "alloc.h"
#include "base64/b64.h"
#include "string_list.h"
#include <assert.h>

int transform_encode_base64(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	char* encoded = NULL;

	encoded = b64_encode(input, input_size);
	*output = encoded; encoded = NULL;
	*output_size = strlen(*output);

	return 0;
}

int reverse_transform_encode_base64(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	*output = b64_decode_ex(input, input_size, output_size);
	return 0;
}

int transform_append(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	char* str = (char*)parameters;
	append(input, input_size, str, strlen(str), output, output_size);
	return 0;
}

int reverse_transform_append(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	uint32_t size = (uint32_t)(UINT_PTR)parameters;
	size_t out_size = input_size - size;
	unsigned char* out = NULL;

	if (out_size > input_size) {
		return 1;
	}

	out = MALLOC(out_size);
	assert(out);

	memcpy(out, input, out_size);
	*output = out; out = NULL;
	*output_size = out_size;

	return 0;
}

int transform_prepend(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	char* str = (char*)parameters;
	append(str, strlen(str), input, input_size, output, output_size);
	return 0;
}

int reverse_transform_prepend(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	uint32_t offset = (uint32_t)(UINT_PTR)parameters;
	size_t out_size = input_size - offset;
	unsigned char* out = NULL;

	if (out_size > input_size) {
		return 1;
	}

	out = MALLOC(out_size);
	assert(out);

	memcpy(out, input + offset, out_size);
	*output = out; out = NULL;
	*output_size = out_size;

	return 0;
}

size_t random_size() {
	size_t size = 0;
	randomnize((unsigned char*)&size, sizeof(size_t));
	return size;
}

static const char alpha_dict[] = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
  'w', 'x', 'y', 'z',
};

int transform_random_alpha(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	unsigned char* data = NULL;
	size_t data_size = 0;
	size_t i = 0;

	data_size = (size_t)parameters;
	data = MALLOC(data_size);

	if (randomnize(data, data_size) != 0) {
		return 1;
	}

	for (i = 0; i < data_size; i++) {
		data[i] = alpha_dict[data[i] % sizeof(alpha_dict)];
	}

	append(input, input_size, data, data_size, output, output_size);
	FREE(data); data = NULL;

	return 0;
}

int reverse_transform_random_alpha(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	return reverse_transform_append(
		parameters,
		input,
		input_size,
		output,
		output_size
	);
}

int transform_choose_random(
	void* parameters,
	const unsigned char* input, size_t input_size,
	unsigned char** output, size_t* output_size
) {
	string_list_t* string_list = NULL;
	size_t pos = 0;

	string_list = (string_list_t*)parameters;
	pos = random_size() % string_list->count;

	*output = copy_string(string_list->strings[pos]);
	*output_size = strlen(*output) + 1;

	return 0;
}


void destroy_append_params(void** params) {
	DESTROY(*params);
}

void destroy_reverse_append_params(void** params) {

}

void destroy_prepend_params(void** params) {
	destroy_append_params(params);
}

void destroy_reverse_prepend_params(void** params) {

}

void destroy_choose_random_params(void** params) {
	string_list_destroy((string_list_t**)params);
}

void destroy_base64_params(void** params) {
}

void destroy_reverse_base64_params(void** params) {

}

void destroy_random_alpha_params(void** params) {
}

void destroy_reverse_random_alpha_params(void** params) {

}

transform_t* transform_new(
	uint16_t type, 
	uint8_t is_reverse, 
	void* parameters, 
	transform_f function
) {
	transform_t* t = MALLOC(sizeof(transform_t));
	assert(t);

	t->type = type;
	t->is_reverse = is_reverse;
	t->parameters = parameters;
	t->function = function;

	return t;
}

void transform_destroy(transform_t** t) {

	switch ((*t)->type) {
	case APPEND_CODE:
		if ((*t)->is_reverse) {
			destroy_reverse_append_params(&(*t)->parameters);
		}
		else {
			destroy_append_params(&(*t)->parameters);
		}
		break;
	case RANDOM_ALPHA_CODE:
		if ((*t)->is_reverse) {
			destroy_reverse_random_alpha_params(&(*t)->parameters);
		}
		else {
			destroy_random_alpha_params(&(*t)->parameters);
		}
		break;
	case BASE64_CODE:
		if ((*t)->is_reverse) {
			destroy_reverse_base64_params(&(*t)->parameters);
		}
		else {
			destroy_base64_params(&(*t)->parameters);
		}
		break;
	case CHOOSE_RANDOM_CODE:
		if ((*t)->is_reverse) {
			assert(0);
		}
		else {
			destroy_choose_random_params(&(*t)->parameters);
		}
		break;
	case PREPEND_CODE:
		if ((*t)->is_reverse) {
			destroy_reverse_prepend_params(&(*t)->parameters);
		}
		else {
			destroy_prepend_params(&(*t)->parameters);
		}
		break;
	}

	(*t)->function = NULL;
	DESTROY(*t);
}

transforms_list_t* transforms_list_new(size_t count, transform_t** transforms) {
	transforms_list_t* tl = MALLOC(sizeof(transforms_list_t));
	assert(tl);

	tl->count = count;
	tl->transforms = transforms;
	return tl;
}

void transforms_list_destroy(transforms_list_t** tl) {
	size_t i = 0;

	for (i = 0; i < (*tl)->count; i++) {
		transform_destroy(&(*tl)->transforms[i]);
	}
	DESTROY((*tl)->transforms);

	DESTROY(*tl);
}

int apply_transforms(
	transforms_list_t* transforms,
	const unsigned char* initial_data,
	size_t initial_data_size,
	unsigned char** output,
	size_t* output_size
) {
	size_t i = 0;
	unsigned char* tmp_in = NULL;
	size_t tmp_in_size = 0;
	unsigned char* tmp_out = NULL;
	size_t tmp_out_size = 0;
	int ok = 0;

	tmp_in = copy_data(initial_data, initial_data_size);
	tmp_in_size = initial_data_size;

	for (i = 0; i < transforms->count; i++) {
		if (transforms->transforms[i]->function(
			transforms->transforms[i]->parameters,
			tmp_in,
			tmp_in_size,
			&tmp_out,
			&tmp_out_size
		) != 0) {
			goto close;
		}
		DESTROY(tmp_in);

		tmp_in = tmp_out;
		tmp_out = NULL;

		tmp_in_size = tmp_out_size;
	}

	*output = tmp_in;
	*output_size = tmp_in_size;
	tmp_in = NULL;

	ok = 1;
close:
	if (tmp_in) DESTROY(tmp_in);
	return ok;
}
