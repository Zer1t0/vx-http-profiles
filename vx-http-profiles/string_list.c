
#include "string_list.h"
#include "alloc.h"
#include <stdint.h>
#include <assert.h>

string_list_t* string_list_new(size_t count, char** strings) {
	string_list_t* sl = MALLOC(sizeof(string_list_t));
	assert(sl);

	sl->count = count;
	sl->strings = strings;

	return sl;
}

void string_list_destroy(string_list_t** sl) {
	size_t i = 0;
	for (i = 0; i < (*sl)->count; i++) {
		DESTROY((*sl)->strings[i]);
	}
	DESTROY((*sl)->strings);
	DESTROY(*sl);
}

wstring_list_t* wstring_list_new(size_t count, wchar_t** strings) {
	wstring_list_t* wl = MALLOC(sizeof(wstring_list_t));
	assert(wl);

	wl->count = count;
	wl->strings = strings;

	return wl;
}

void wstring_list_destroy(wstring_list_t** wl) {
	size_t i = 0;
	for (i = 0; i < (*wl)->count; i++) {
		DESTROY((*wl)->strings[i]);
	}
	DESTROY((*wl)->strings);
	DESTROY(*wl);
}

void wstring_list_append(wstring_list_t* wl, wchar_t* str) {
	size_t new_size = (wl->count + 1) * sizeof(wchar_t*);
	wchar_t** tmp = NULL;

	tmp = RECALLOC(wl->strings, new_size);
	assert(tmp);
	wl->strings = tmp;
	wl->strings[wl->count] = str;
	wl->count++;
}