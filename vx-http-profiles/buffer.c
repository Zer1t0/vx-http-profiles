
#include "buffer.h"
#include "alloc.h"
#include "basic_utils.h"

#include <assert.h>

buffer_t* buffer_new(unsigned char* data, size_t size) {
	buffer_t* b = MALLOC(sizeof(buffer_t));
	assert(b);

	b->data = data;
	b->size = size;
	return b;
}

buffer_t* buffer_new_copy(unsigned char* data, size_t size) {
	return buffer_new(copy_data(data, size), size);
}

void buffer_destroy(buffer_t** b) {
	DESTROY((*b)->data);
	DESTROY(*b);
}

buffer_t* buffer_from_string(char* str) {
	size_t size = strlen(str) + 1;
	return buffer_new(str, size);
}