# vx-http-profiles

This is a project to create a example of c client generating and parsing different HTTP messages based on the given configuration.

I think the most interesting parts are:

- `agent_config.json`: The configuration for the HTTP communication. Based in Mythic [dynamichttp](https://github.com/MythicC2Profiles/dynamichttp) profile configuration.

- `gen-http-config.py`: Generates the HTTP configuration in a binary mode that is interpreted by the c client to generate/parse HTTP message. This is the part to change for adapting the client to new http-profiles format, like Cobalt Strike.

- `vx-http-profiles/http_pipes_parse.c`: Agent configuration parser. Loads the configuration in binary mode.

- `vx-http-profiles/http_pipes_process.c`: Generates the HTTP requests and parse the HTTP responses to extract the payload.

- `vx-http-profiles/transforms.c`: Include the transforms supported by the agent.

- `test-server.py`: A little Python server that is given the same HTTP configuration as the agent and can talk with it.

## Transforms

Currently, the following transforms are supported:

- append: Appends the parameter to the given value.
- prepend: Prepends the parameter to the given value.
- base64: Encodes the value in base64.
- random_alpha: Generates a random string with the given length in parameter and appends it to the value.
- choose_random: Chooses a string between the given ones and appends it to the value.


## Example

With the given configuration, the program runs like this for a `GET` request:

```
PS C:\> .\vx-http-profiles.exe
==== HTTP Request ====
Uri: /jquery-3.3.1.min.js?q=Hello world
Header -> Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Header -> Host: code.jquery.com
Header -> Referer: http://code.jquery.com/
Header -> Accept-Encoding: gzip, deflate
Header -> User-Agent: Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko
Header -> Cookie: __cfduid = AFRDUWxVb1BKWWpNaVBiTW5EeldsTXRkU2xYWHFhag==
Body (0):
=======================
==== HTTP Response ====
Header -> Cache-Control: max-age=0, no-cache
Header -> Date: Sun, 09 Apr 2023 12:47:06 GMT
Header -> Pragma: no-cache
Header -> Content-Length: 56
Header -> Content-Type: text/html; charset=utf-8
Header -> Server: BaseHTTP/0.6 Python/3.11.1
Header -> Server: NetDNA-cache/2.2
Body (56): <html>Hello worldU2VydmVyIEdFVDogSGVsbG8gd29ybGQ=</html>
=======================
Payload (23): Server GET: Hello world
```
